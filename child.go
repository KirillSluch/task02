//!/usr/bin/env go run
package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"syscall"
	"time"
)

func main() {
	arg, _ := strconv.Atoi(os.Args[1])
	s := time.Duration(arg)

	myPID := syscall.Getpid()
	parPID := syscall.Getppid()

	fmt.Printf("Child[%v]: I started. My PID %v. Parent PID %v.\n", myPID, myPID, parPID)

	time.Sleep(s * time.Second)

	fmt.Printf("Child[%v]: I am ended. PID %v. Parent PID %v.\n", myPID, myPID, parPID)
	rand.Seed(time.Now().UnixNano())
	code := rand.Intn(2)
	syscall.Exit(code)
}
