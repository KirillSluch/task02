package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"syscall"
	"time"
)

func main() {
	args := &syscall.ProcAttr{
		Env:   []string{},
		Files: []uintptr{os.Stdin.Fd(), os.Stdout.Fd(), os.Stderr.Fd()},
	}
	if len(os.Args) < 2 {
		fmt.Println("Enter command line arg")
		return
	}
	n, _ := strconv.Atoi(os.Args[1])

	execQueue := make([]int, 0)
	myPID := syscall.Getpid()
	rand.Seed(time.Now().UnixNano())

	execQueue = runNProcesses(n, args, myPID, execQueue)

	for len(execQueue) > 0 {
		childPID := execQueue[0]
		execQueue = execQueue[1:]

		var status syscall.WaitStatus
		_, err := syscall.Wait4(childPID, &status, 0, nil)

		if err != nil {
			fmt.Println("Error: ", err)
		}

		exitStatus := status.ExitStatus()
		fmt.Printf("Parent[%v]: Child with PID %v terminated. Exit Status %v.\n", myPID, childPID, exitStatus)

		if exitStatus != 0 {
			childPID := execChild(args)
			fmt.Printf("Parent[%v]: I ran children process with PID %v.\n", myPID, childPID)
			execQueue = append(execQueue, childPID)
		}
	}
}

func runNProcesses(n int, args *syscall.ProcAttr, myPID int, execQueue []int) []int {
	for i := 0; i < n; i++ {
		childPID := execChild(args)

		fmt.Printf("Parent[%v]: I ran children process with PID %v.\n", myPID, childPID)
		execQueue = append(execQueue, childPID)
	}
	return execQueue
}

func execChild(args *syscall.ProcAttr) int {
	commandLineArgs := []string{"./child"}
	commandLineArgs = append(commandLineArgs, generateSleepTime())
	childPID, _ := syscall.ForkExec("./child", commandLineArgs, args)
	return childPID
}

func generateSleepTime() string {
	seconds := []string{"5", "6", "7", "8", "9", "10"}
	return seconds[rand.Intn(5)]
}
